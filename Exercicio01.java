/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio01;

/**
 *
 * @author alunoti
 */
public class Exercicio01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Declaração das variáveis
        
        int num1, num2, soma, subtracao ,multiplicacao;
        float divisao;
        
        // Atribuindo um valor as variáveis
        
        num1 = 15; 
        num2 = 26;
        soma = num1 + num2;
        subtracao = num1 - num2;
        multiplicacao = num1 * num2;
        divisao = (float) num1 / num2;
        
        //Imprimindo o valor das variáveis
        
        System.out.println("O valor da variável num1 é: " + num1);
        System.out.println("O valor da variável num2 é: " + num2);
        System.out.println("O valor da variável soma: " + soma);
        System.out.println("O valor da variável subtração: " + subtracao);
        System.out.println("O valor da variável multiplicação: " + multiplicacao);
        System.out.println("O valor da variável divisão: " + divisao);
        
    }
    
}
